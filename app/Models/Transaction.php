<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'type',
        'transaction_id',
        'current',
        'add',
        'final',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function topupHistory()
    {
        return $this->hasOne(TopupHistory::class, 'transaction_id', 'id');
    }

    public function orderHistory()
    {
        return $this->hasOne(OrderHistory::class, 'transaction_id', 'id');
    }
}
