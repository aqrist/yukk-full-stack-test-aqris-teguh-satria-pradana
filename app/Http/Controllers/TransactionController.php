<?php

namespace App\Http\Controllers;

use App\Models\OrderHistory;
use App\Models\TopupHistory;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Get user id
        $userid = Auth::user()->id;

        $query = Transaction::with(['topupHistory', 'orderHistory'])
            ->where('user_id', $userid)
            ->reorder('updated_at', 'desc')->get();

        return view('pages.transactions.index', compact('query'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate the request
        $validated = $request->validate([
            'type' => 'required|in:topup,order',
            'add' => 'required|numeric',
            'description' => 'required',
            'image' => 'nullable|image|max:2048' // Maximum image size is 2MB
        ]);

        // Get user id
        $userid = Auth::user()->id;

        // If 'type' is 'topup' and 'image' is provided, handle the image upload
        if ($validated['type'] === 'topup' && $request->hasFile('image')) {
            $imagePath = $request->file('image')->store('images/topup', 'public');
        }

        // Calculate the 'final' value based on the 'current' value of the latest transaction from current user
        $latestTransaction = Transaction::where('user_id', $userid)
            ->latest('created_at')->first();
        $current = $latestTransaction ? $latestTransaction->final : 0;

        // Adjust the 'add' amount based on the transaction type (subtract for 'order' type)
        $add = $validated['add'];
        if ($validated['type'] === 'order') {
            $add *= -1; // Multiply by -1 to subtract the value
        }

        $final = $current + $add;

        // Create a new transaction with the validated data
        $transaction = new Transaction([
            'user_id' => $userid,
            'type' => $validated['type'],
            'current' => $current,
            'add' => $add,
            'final' => $final,
        ]);

        // Generate a unique ref_id based on the transaction type
        $ref_id_prefix = $validated['type'] === 'topup' ? 'T' : 'O';
        $uniqueid = strtoupper(Str::random(6));
        $ref_id = $ref_id_prefix . $uniqueid;

        // Save the transaction record to the database
        $transaction->ref_id = $ref_id; // Assign ref_id to the generated unique ID
        $transaction->save();

        // Check if 'type' is 'topup'. If yes, create a topup history record.
        if ($validated['type'] === 'topup') {
            $topupHistory = new TopupHistory([
                'amount' => $validated['add'],
                'description' => $request['description'],
                'transaction_id' => $transaction->id,
                'image_path' => $imagePath ?? null // Save the image path if it exists
            ]);
            $topupHistory->save(); // Save the record to the database
        }

        // Check if 'type' is 'order'. If yes, create an order history record.
        if ($validated['type'] === 'order') {
            $orderHistory = new OrderHistory([
                'amount' => $validated['add'],
                'description' => $request['description'],
                'transaction_id' => $transaction->id
            ]);
            $orderHistory->save(); // Save the record to the database
        }

        if ($request->has('save_only')) {
            // code to back and save
            return back()->with('success', 'Transaction added Succesfully!');
        } elseif ($request->has('save_and_back')) {
            // code to exit and save
            return redirect()->route('transactions.index')->with('success', 'Transaction added Succesfully!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        // Find the transaction with the given ID
        $transaction = Transaction::with(['user'])->findOrFail($id);

        // Check if 'type' is 'topup'. If yes, get topup history record.
        if ($transaction->type === 'topup') {
            $history = $transaction->topupHistory;
        }

        // Check if 'type' is 'order'. If yes, get order history record.
        if ($transaction->type === 'order') {
            $history = $transaction->orderHistory;
        }

        // Return the transaction and history to the view
        return view('pages.transactions.show', compact('transaction', 'history'));
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
