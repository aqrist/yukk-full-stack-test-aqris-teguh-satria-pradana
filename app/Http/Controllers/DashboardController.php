<?php

namespace App\Http\Controllers;

use App\Models\OrderHistory;
use App\Models\TopupHistory;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $userid = Auth::user()->id;

        $total_transaction = Transaction::where('user_id', $userid)->count();

        $total_amount = TopupHistory::with('transaction')
            ->whereHas('transaction', function ($query) use ($userid) {
                $query->where('user_id', $userid);
            })
            ->sum('amount');

        $total_spent = OrderHistory::with('transaction')
            ->whereHas('transaction', function ($query) use ($userid) {
                $query->where('user_id', $userid);
            })
            ->sum('amount');

        return view('home', compact('total_transaction', 'total_amount', 'total_spent'));
    }
}
