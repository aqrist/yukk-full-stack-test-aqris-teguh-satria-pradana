@extends('layouts.sb')

@section('heading')
    Transactions
@endsection

@section('content')
    {{-- notification --}}
    @include('components/flash-messages')
    {{-- end notification --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">

                <div class="card-header">
                    Add new Transactions
                </div>

                <div class="card-body">
                    <form action="{{ route('transactions.store') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <!-- Create row then inside there are 2 col md 6 -->
                        <!-- Type & Amount -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="type" class="form-control-label">Type</label>
                                    <select name="type" id="type" class="form-control" required>
                                        <option value="" disabled selected>-- Select Type --</option>
                                        <option value="topup">Topup</option>
                                        <option value="order">Order</option>
                                    </select>
                                    @error('type')
                                        <div class="text-muted">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="add" class="form-control-label">Amount</label>
                                    <input type="number" class="form-control" name="add" id=""
                                        value="{{ old('add') }}" required>
                                    @error('add')
                                        <div class="text-muted">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description" class="form-control-label">Description</label>
                                    <input type="text" class="form-control" name="description" id=""
                                        value="{{ old('description') }}" required>
                                    @error('description')
                                        <div class="text-muted">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                             <!-- Add file input for image -->
                             <div class="col-md-12">
                                <div class="form-group" style="display: none">
                                    <label for="image" class="form-control-label">Image</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                    @error('image')
                                        <div class="text-muted">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="save_only" class="btn btn-primary">Save</button>
                            <button type="submit" name="save_and_back" class="btn btn-info">Save and Back</button>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script>
        document.getElementById('type').addEventListener('change', function() {
            var addInput = document.querySelector('input[name="add"]');
            var descriptionInput = document.querySelector('input[name="description"]');
            var imageInput = document.querySelector('input[name="image"]');
            var type = this.value;

            // Hide the image input if the type is 'order', else show it
            if (type === 'order') {
                imageInput.closest('.form-group').style.display = 'none';
                addInput.required = true;
                descriptionInput.required = true;
            } else {
                imageInput.closest('.form-group').style.display = 'block';
                addInput.required = false;
                descriptionInput.required = false;
            }
        });
    </script>
@endpush
