@extends('layouts.sb')

@section('heading')
    Details Transaction
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Transaction Details</div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="ref_id" class="col-md-4 col-form-label text-md-right">Transaction ID</label>
                            <div class="col-md-6">
                                <input id="ref_id" type="text" class="form-control" name="ref_id"
                                    value="{{ $transaction->ref_id }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">Username / email</label>
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username"
                                    value="{{ $transaction->user->name }} / {{ $transaction->user->email }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control" name="type"
                                    value="{{ $transaction->type }}" readonly>
                            </div>
                        </div>

                        @if (isset($history))
                            <div class="form-group row">
                                <label for="amount" class="col-md-4 col-form-label text-md-right">Amount</label>
                                <div class="col-md-6">
                                    <input id="amount" type="text" class="form-control" name="amount"
                                        value="{{ $history->amount }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description"
                                        value="{{ $history->description }}" readonly>
                                </div>
                            </div>

                            @if ($transaction->type == 'topup')
                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">Proof
                                        Image</label>
                                    <div class="col-md-6">
                                        <img src="{{ asset('storage/' . $history->image_path) }}" alt="Transaction Image"
                                            class="img-fluid">

                                    </div>
                                </div>
                            @else
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
