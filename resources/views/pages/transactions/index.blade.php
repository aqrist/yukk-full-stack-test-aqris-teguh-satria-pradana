@extends('layouts.sb')

@section('heading')
    Transactions
@endsection

@section('content')
    {{-- notification --}}
    @include('components/flash-messages')
    {{-- end notification --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('transactions.create') }}" class="btn btn-primary mb-3">
                Create Transaction
            </a>
        </div>

        <div class="col-md-12">
            <div class="card shadow">

                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Transactions</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover scroll-horizontal-vertical" id="crudTable"
                            width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="d-none">Id</th>
                                    <th>Trx Code</th>
                                    <th>Type</th>
                                    <th>Desc</th>
                                    <th>Current balance</th>
                                    <th>Amount</th>
                                    <th>Final Balance</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($query as $item)
                                    <tr>
                                        <td class="d-none">{{ $item->id }}</td>
                                        <td>{{ $item->ref_id }}</td>
                                        <td>{{ $item->type }}</td>
                                        @if ($item->type == 'topup')
                                            <td>{{ $item->topupHistory->description }}</td>
                                        @else
                                            <td>{{ $item->orderHistory->description }}</td>
                                        @endif
                                        <td>{{ $item->current }}</td>
                                        <td>{{ $item->add }}</td>
                                        <td>{{ $item->final }}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('transactions.show', $item->id) }}">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script>
        // AJAX DataTable
        var datatable = $('#crudTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excelHtml5',
                title: 'transaction_export',
                filename: 'transaction_export',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6] // Exclude columns 0 (Id) and 7 (Action)
                },
            }, ],
            paging: true,
            pageLength: 10,
            order: [[ 0, "desc" ]]
        });
    </script>
@endpush
