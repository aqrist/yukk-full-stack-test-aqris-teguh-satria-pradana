# Yukk

simple app using Laravel 10.

## Installation
Install dependencies using Composer.
```bash
composer install
```
Generate an application key.
```bash
php artisan key:generate
```
Perform migrations to create necessary tables.
```bash
php artisan migrate
```
Install NPM.
```bash
npm install && npm run build
```
Run the application.
```bash
php artisan serve
```

## Configuration
need to add email credentials for reset password, and sentry DSN to use sentry

## Contributing

This project is open for contributions. Feel free to fork the repository, make your changes, and submit a pull request.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
